using AutoMapper;
using ConceptMap.Web.Models.Service.Dictionary;
using ConceptMap.Web.Infrastructure;
using ConceptMap.Web.Service.Wikipedia;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;

namespace ConceptMap.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("MyAllowList",
                builder =>
                {
                    builder.WithOrigins("http://localhost:8080", "https://knowstuff.azurewebsites.net");
                });

            });

            services.AddSwaggerDocument();

            services.AddControllers();
            services.AddAutoMapper(c => c.AddProfile<AutoMapperDictionaryProfile>(), typeof(DictionarySettings));

            services.AddSingleton<IDictionarySettings, DictionarySettings>(sp => Configuration.GetSection("OxfordDictionary").Get<DictionarySettings>());
            services.AddHttpClient<IDictionaryService, DictionaryService>();

            services.AddSingleton<IWikipediaFunctionSettings, WikipediaFunctionSettings>(sp => Configuration.GetSection("WikipediaFunction").Get<WikipediaFunctionSettings>());
            services.AddHttpClient<IWikipediaService, WikipediaService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();            
            app.UseCors("MyAllowList");
            app.UseRouting();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            // Register the Swagger generator and the Swagger UI middlewares
            app.UseOpenApi();
            app.UseSwaggerUi3();
        }
    }
}
