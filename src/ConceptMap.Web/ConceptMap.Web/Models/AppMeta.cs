﻿using System;

namespace ConceptMap.Web.Models
{
    public partial class AppController
    {
        public class AppMetaData
        {
            public string Message { get; set; }
            public string Version { get; set; }

            public AppMetaData()
            {
                Version = MakeVersionFromDate();
                Message = "Up, up and away!";
            }

            private string MakeVersionFromDate()
            {
                var pacificTime = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "Pacific Standard Time");
                return pacificTime.ToShortDateString() + " " + pacificTime.ToShortTimeString();
            }
        }
    }
}