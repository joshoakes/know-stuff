﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConceptMap.Web.Models
{
    public class LemmaDto
    {
        public string Word { get; set; }
        public List<LemmaLexicalEntryDto> LexicalEntries { get; set; } = new List<LemmaLexicalEntryDto>();
    }

    public class LemmaLexicalEntryDto
    {
        public string LexicalCategory { get; set; }
        public List<string> InflectionOf { get; set; } = new List<string>();        
    }
}
