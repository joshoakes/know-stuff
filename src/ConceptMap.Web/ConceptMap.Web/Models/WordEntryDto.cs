﻿using System.Collections.Generic;

namespace ConceptMap.Web.Models
{
    public class WordEntryDto
    {
        public string Word { get; set; }
        public List<LexicalEntriesDto> LexicalEntries { get; set; } = new List<LexicalEntriesDto>();
    }

    public class LexicalEntriesDto
    {
        public string LexicalCategory { get; set; }
        public List<EntriesDto> Entries { get; set; } = new List<EntriesDto>();
    }

    public class EntriesDto
    {
        public List<SensesDto> Senses { get; set; } = new List<SensesDto>();
    }
    
    public class SensesDto
    {
        public string Definition { get; set; }
        public string Example { get; set; }
        public List<SubSenseDto> SubSenses { get; set; } = new List<SubSenseDto>();
        public List<string> Synonyms { get; set; } = new List<string>();
    }

    public class SubSenseDto
    {
        public string Definition { get; set; }
        public List<string> Domains { get; set; } = new List<string>();
    }
}
