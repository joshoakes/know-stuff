﻿namespace ConceptMap.Web.Infrastructure
{
    public interface IDictionarySettings
    {
        string AppId { get; }
        string AppKey { get; }
        string Uri { get; }
    }
}