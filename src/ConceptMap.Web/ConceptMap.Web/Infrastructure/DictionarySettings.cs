﻿namespace ConceptMap.Web.Infrastructure
{
    public class DictionarySettings : IDictionarySettings
    {
        public string Uri { get; set; }
        public string AppId { get; set; }
        public string AppKey { get; set; }
    }
}
