﻿using AutoMapper;
using ConceptMap.Web.Models;
using ConceptMap.Web.Service.Dictionary.Model;
using System.Linq;

namespace ConceptMap.Web.Infrastructure
{
    public class AutoMapperDictionaryProfile : Profile
    {
        public AutoMapperDictionaryProfile()
        {
            // Entry
            CreateMap<EntryResult, WordEntryDto>()
                .ForMember(dest => dest.Word, opt => opt.MapFrom(src => src.Word));

            CreateMap<EntryLexicalEntry, LexicalEntriesDto>()
                .ForMember(dest => dest.LexicalCategory, opt => opt.MapFrom(src => src.LexicalCategory.Text));

            CreateMap<Entry, EntriesDto>();

            CreateMap<Sense, SensesDto>()
                .ForMember(dest => dest.Definition, opt => opt.MapFrom(src => src.Definitions.FirstOrDefault()))
                .ForMember(dest => dest.Example, opt => opt.MapFrom(src => src.Examples.FirstOrDefault().Text))
                .ForMember(dest => dest.Synonyms, opt => opt.MapFrom(src => src.Synonyms.Select(s => s.Text)));

            CreateMap<Subsense, SubSenseDto>()
                .ForMember(dest => dest.Definition, opt => opt.MapFrom(src => src.Definitions.FirstOrDefault()))
                .ForMember(dest => dest.Domains, opt => opt.MapFrom(src => src.Domains.Select(d => d.Text)));

            // Lemma
            CreateMap<LemmaResult, LemmaDto>()
                .ForMember(dest => dest.Word, opt => opt.MapFrom(src => src.Word));

            CreateMap<LemmaLexicalEntry, LemmaLexicalEntryDto>()
                .ForMember(dest => dest.LexicalCategory, opt => opt.MapFrom(src => src.LexicalCategory.Text))
                .ForMember(dest => dest.InflectionOf, opt => opt.MapFrom(src => src.InflectionOf.Select(i => i.Text)));
        }
    }
}
