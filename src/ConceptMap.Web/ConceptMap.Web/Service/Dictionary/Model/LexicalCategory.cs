﻿using System.Text.Json.Serialization;

namespace ConceptMap.Web.Service.Dictionary.Model
{
    public class LexicalCategory
    {
        [JsonPropertyName("id")]
        public string Id { get; set; }

        [JsonPropertyName("text")]
        public string Text { get; set; }
    }
}