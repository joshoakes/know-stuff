﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace ConceptMap.Web.Service.Dictionary.Model
{
    public partial class LemmaResponse
    {
        [JsonPropertyName("metadata")]
        public Metadata Metadata { get; set; }

        [JsonPropertyName("results")]
        public List<LemmaResult> Results { get; set; } = new List<LemmaResult>();
    }

    public partial class Metadata
    {
        [JsonPropertyName("provider")]
        public string Provider { get; set; }
    }

    public class LemmaResult
    {
        [JsonPropertyName("id")]
        public string Id { get; set; }

        [JsonPropertyName("language")]
        public string Language { get; set; }

        [JsonPropertyName("lexicalEntries")]
        public List<LemmaLexicalEntry> LexicalEntries { get; set; } = new List<LemmaLexicalEntry>();

        [JsonPropertyName("word")]
        public string Word { get; set; }
    }

    public class LemmaLexicalEntry
    {
        [JsonPropertyName("inflectionOf")]
        public List<LexicalCategory> InflectionOf { get; set; } = new List<LexicalCategory>();

        [JsonPropertyName("language")]
        public string Language { get; set; }

        [JsonPropertyName("lexicalCategory")]
        public LexicalCategory LexicalCategory { get; set; }

        [JsonPropertyName("text")]
        public string Text { get; set; }
    }
}