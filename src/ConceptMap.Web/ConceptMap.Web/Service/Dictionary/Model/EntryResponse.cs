﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace ConceptMap.Web.Service.Dictionary.Model
{
    public class EntryResponse
    {
        [JsonPropertyName("id")]
        public string Id { get; set; }

        [JsonPropertyName("metadata")]
        public EntryMetadata Metadata { get; set; }

        [JsonPropertyName("results")]
        public List<EntryResult> Results { get; set; } = new List<EntryResult>();

        [JsonPropertyName("word")]
        public string Word { get; set; }
    }

    public class EntryMetadata
    {
        [JsonPropertyName("operation")]
        public string Operation { get; set; }

        [JsonPropertyName("provider")]
        public string Provider { get; set; }

        [JsonPropertyName("schema")]
        public string Schema { get; set; }
    }

    public class EntryResult
    {
        [JsonPropertyName("id")]
        public string Id { get; set; }

        [JsonPropertyName("language")]
        public string Language { get; set; }

        [JsonPropertyName("lexicalEntries")]
        public List<EntryLexicalEntry> LexicalEntries { get; set; } = new List<EntryLexicalEntry>();

        [JsonPropertyName("type")]
        public string Type { get; set; }

        [JsonPropertyName("word")]
        public string Word { get; set; }
    }

    public class EntryLexicalEntry
    {
        [JsonPropertyName("derivatives")]
        public List<LexicalCategory> Derivatives { get; set; } = new List<LexicalCategory>();

        [JsonPropertyName("entries")]
        public List<Entry> Entries { get; set; } = new List<Entry>();

        [JsonPropertyName("language")]
        public string Language { get; set; }

        [JsonPropertyName("lexicalCategory")]
        public LexicalCategory LexicalCategory { get; set; }

        [JsonPropertyName("pronunciations")]
        public List<Pronunciation> Pronunciations { get; set; } = new List<Pronunciation>();

        [JsonPropertyName("text")]
        public string Text { get; set; }
    }

    public partial class Entry
    {
        [JsonPropertyName("etymologies")]
        public List<string> Etymologies { get; set; } = new List<string>();

        [JsonPropertyName("senses")]
        public List<Sense> Senses { get; set; } = new List<Sense>();
    }

    public partial class Sense
    {
        [JsonPropertyName("definitions")]
        public List<string> Definitions { get; set; } = new List<string>();

        [JsonPropertyName("examples")]
        public List<Example> Examples { get; set; } = new List<Example>();

        [JsonPropertyName("id")]
        public string Id { get; set; }

        [JsonPropertyName("shortDefinitions")]
        public List<string> ShortDefinitions { get; set; } = new List<string>();

        [JsonPropertyName("subsenses")]
        public List<Subsense> Subsenses { get; set; } = new List<Subsense>();

        [JsonPropertyName("synonyms")]
        public List<Synonym> Synonyms { get; set; } = new List<Synonym>();

        [JsonPropertyName("thesaurusLinks")]
        public List<ThesaurusLink> ThesaurusLinks { get; set; } = new List<ThesaurusLink>();
    }

    public partial class Example
    {
        [JsonPropertyName("text")]
        public string Text { get; set; }
    }

    public partial class Subsense
    {
        [JsonPropertyName("definitions")]
        public List<string> Definitions { get; set; } = new List<string>();

        [JsonPropertyName("domains")]
        public List<LexicalCategory> Domains { get; set; } = new List<LexicalCategory>();

        [JsonPropertyName("id")]
        public string Id { get; set; }

        [JsonPropertyName("shortDefinitions")]
        public List<string> ShortDefinitions { get; set; } = new List<string>();
    }

    public class Synonym
    {
        [JsonPropertyName("id")]
        public string Id { get; set; }

        [JsonPropertyName("language")]
        public string Language { get; set; }

        [JsonPropertyName("text")]
        public string Text { get; set; }
    }

    public class ThesaurusLink
    {
        [JsonPropertyName("entry_id")]
        public string EntryId { get; set; }

        [JsonPropertyName("sense_id")]
        public string SenseId { get; set; }
    }

    public partial class Pronunciation
    {
        [JsonPropertyName("dialects")]
        public List<string> Dialects { get; set; } = new List<string>();

        [JsonPropertyName("phoneticNotation")]
        public string PhoneticNotation { get; set; }

        [JsonPropertyName("phoneticSpelling")]
        public string PhoneticSpelling { get; set; }

        [JsonPropertyName("audioFile")]
        public Uri AudioFile { get; set; }
    }
}
