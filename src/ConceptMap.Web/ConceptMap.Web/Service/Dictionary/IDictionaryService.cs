﻿using ConceptMap.Web.Service.Dictionary.Model;
using System.Threading.Tasks;

namespace ConceptMap.Web.Models.Service.Dictionary
{
    public interface IDictionaryService
    {
        Task<LemmaResponse> LookupLemma(string word);
        Task<EntryResponse> LookupEntry(string word);
        Task<object> LookupWord(string word);
    }
}