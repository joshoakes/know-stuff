﻿using ConceptMap.Web.Infrastructure;
using ConceptMap.Web.Service.Dictionary.Model;
using System;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace ConceptMap.Web.Models.Service.Dictionary
{
    public class DictionaryService : IDictionaryService
    {
        private readonly HttpClient _client;        

        public DictionaryService(HttpClient client, IDictionarySettings dictionarySettings)
        {
            client.BaseAddress = new Uri(dictionarySettings.Uri);
            client.DefaultRequestHeaders.Add("app_id", dictionarySettings.AppId);
            client.DefaultRequestHeaders.Add("app_key", dictionarySettings.AppKey);

            _client = client;
        }

        public async Task<EntryResponse> LookupEntry(string word)
        {            
            var route = "/api/v2/entries/en-us/" + word + "?strictMatch=false";            
            var response = await _client.GetAsync(route);
            if (response.IsSuccessStatusCode)
            {
                // var res = await response.Content.ReadAsStringAsync();
                using var responseStream = await response.Content.ReadAsStreamAsync();
                return await JsonSerializer.DeserializeAsync<EntryResponse>(responseStream);
            }

            return null;
        }

        public async Task<LemmaResponse> LookupLemma(string word)
        {
            var route = "/api/v2/lemmas/en-us/" + word;
            var response = await _client.GetAsync(route);
            if (response.IsSuccessStatusCode)
            {                
                using var responseStream = await response.Content.ReadAsStreamAsync();
                return await JsonSerializer.DeserializeAsync<LemmaResponse>(responseStream);
            }

            return null;
        }

        /// <summary>
        /// 403 Forbidden - requires higher plan at https://developer.oxforddictionaries.com/
        /// </summary>
        /// <param name="word"></param>
        /// <returns></returns>
        public async Task<object> LookupWord(string word)
        {
            var route = "/api/v2/words/en-us?q=" + word;
            var response = await _client.GetAsync(route);
            if (response.IsSuccessStatusCode)
            {
                using var responseStream = await response.Content.ReadAsStreamAsync();
                return await JsonSerializer.DeserializeAsync<object>(responseStream);
            }

            return null;
        }
    }
}