﻿using ConceptMap.Web.Service.Wikipedia.Model;
using System;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace ConceptMap.Web.Service.Wikipedia
{

    public class WikipediaService : IWikipediaService
    {
        private readonly HttpClient _httpClient;
        private readonly IWikipediaFunctionSettings _wikiSettings;

        public WikipediaService(HttpClient httpClient, IWikipediaFunctionSettings wikiSettings)
        {            
            _wikiSettings = wikiSettings;
            _httpClient = httpClient;
            _httpClient.BaseAddress = new Uri(wikiSettings.BaseUri);
        }

        public async Task<WikiPageResult> GetPageAsync(string title)
        {
            var route = $"/api/WikiPage?code={_wikiSettings.PageCode}&title={title}";
            var response = await _httpClient.GetAsync(route);
            using var responseStream = await response.Content.ReadAsStreamAsync();
            if (response.IsSuccessStatusCode)
            {                
                var article = await JsonSerializer.DeserializeAsync<WikiArticle>(responseStream);
                return WikiPageResult.Success(article);
            }
            
            if (response.StatusCode == System.Net.HttpStatusCode.Conflict)
            {
                return WikiPageResult.Conflict;
            }

            return WikiPageResult.Error;
        }

        public async Task<WikiSearchResult> SearchTermAsync(string query)
        {
            var route = $"/api/WikiSearch?code={_wikiSettings.SearchCode}&query={query}";
            var response = await _httpClient.GetAsync(route);
            if (response.IsSuccessStatusCode)
            {
                using var responseStream = await response.Content.ReadAsStreamAsync();
                return await JsonSerializer.DeserializeAsync<WikiSearchResult>(responseStream);
            }

            return null;
        }
    }
}
