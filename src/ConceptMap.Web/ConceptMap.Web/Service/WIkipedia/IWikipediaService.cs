﻿using ConceptMap.Web.Service.Wikipedia.Model;
using System.Threading.Tasks;

namespace ConceptMap.Web.Service.Wikipedia
{
    public interface IWikipediaService
    {
        Task<WikiSearchResult> SearchTermAsync(string query);
        Task<WikiPageResult> GetPageAsync(string page);
    }
}
