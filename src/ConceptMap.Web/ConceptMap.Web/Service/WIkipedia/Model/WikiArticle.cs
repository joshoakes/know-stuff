﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace ConceptMap.Web.Service.Wikipedia.Model
{
    public class WikiPageResult
    {
        public WikiArticle Article { get; private set; }
        public bool Conflicted { get; private set; }
        public bool Errored { get; private set; }
        public bool Succeeded => Article != null && Conflicted == false && Errored == false;

        private WikiPageResult() { }

        public static WikiPageResult Success(WikiArticle article)
        {
            return new WikiPageResult()
            {
                Article = article,
                Conflicted = false,
                Errored = false
            };
        }

        public static WikiPageResult Error => new WikiPageResult()
        {
            Article = null,
            Conflicted = false,
            Errored = true
        };

        public static WikiPageResult Conflict =>
        new WikiPageResult()
        {
            Article = null,
            Conflicted = true,
            Errored = false
        };
    }

    public class WikiArticle
    {
        [JsonPropertyName("title")]
        public string Title { get; set; }

        [JsonPropertyName("url")]
        public string Url { get; set; }

        [JsonPropertyName("images")]
        public List<string> Images { get; set; }

        [JsonPropertyName("summary")]
        public List<string> Summary { get; set; }

        [JsonPropertyName("links")]
        public List<string> Links { get; set; }
    }
}
