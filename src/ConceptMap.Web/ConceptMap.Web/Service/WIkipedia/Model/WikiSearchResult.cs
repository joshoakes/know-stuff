﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace ConceptMap.Web.Service.Wikipedia.Model
{
    public class WikiSearchResult
    {
        [JsonPropertyName("suggested")]
        public string Suggested { get; set; }

        [JsonPropertyName("pages")]
        public List<string> Pages { get; set; }
    }
}
