﻿namespace ConceptMap.Web.Service.Wikipedia
{
    public class WikipediaFunctionSettings : IWikipediaFunctionSettings
    {
        public string BaseUri { get; set; }
        public string SearchCode { get; set; }
        public string PageCode { get; set; }
    }
}
