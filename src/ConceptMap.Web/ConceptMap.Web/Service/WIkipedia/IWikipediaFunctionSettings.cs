﻿namespace ConceptMap.Web.Service.Wikipedia
{
    public interface IWikipediaFunctionSettings
    {
        string BaseUri { get; }
        string PageCode { get; }
        string SearchCode { get; }
    }
}