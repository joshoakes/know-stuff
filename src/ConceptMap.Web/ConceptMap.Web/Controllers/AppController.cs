﻿using Microsoft.AspNetCore.Mvc;

namespace ConceptMap.Web.Models
{
    [ApiController]
    [Route("api/[controller]")]    
    [Produces("application/json")]
    public partial class AppController : ControllerBase
    {
        [HttpGet("/")]
        public ActionResult<AppMetaData> App()
        {
            return Ok(new AppMetaData());
        }
    }
}