﻿using ConceptMap.Web.Service.Wikipedia;
using ConceptMap.Web.Service.Wikipedia.Model;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace ConceptMap.Web.Models
{
    [ApiController]
    [Route("api/v0/wiki")]    
    [Produces("application/json")]
    public class WikiController : ControllerBase
    {
        private readonly IWikipediaService _wikiService;

        public WikiController(IWikipediaService wikiService)
        {
            _wikiService = wikiService;
        }

        [HttpGet("search")]
        public async Task<ActionResult<WikiSearchResult>> SearchWiki([FromQuery] string q)
        {
            var result = await _wikiService.SearchTermAsync(q);
            if (result == null)
                return BadRequest($"Search not found {q}");

            return Ok(result);
        }

        [HttpGet("page/{title}")]
        public async Task<ActionResult<WikiArticle>> GetPage(string title)
        {
            var result = await _wikiService.GetPageAsync(title);
            if (result == null || result.Errored)
            {
                return StatusCode(500, "Uxpected error");
            }

            if (result.Conflicted)
            {
                return Conflict();
            }

            return Ok(result.Article);
        }
    }
}