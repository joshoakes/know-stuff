﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using AutoMapper;
using ConceptMap.Web.Models.Service.Dictionary;
using ConceptMap.Web.Service.Dictionary.Model;
using Microsoft.AspNetCore.Mvc;

namespace ConceptMap.Web.Models
{
    [ApiController]
    [Route("api/dictionary")]    
    [Produces("application/json")]
    public class DictionaryController : ControllerBase
    {
        private IDictionaryService _dictionaryService;
        private readonly IMapper _mapper;

        public DictionaryController(IDictionaryService dictionaryService, IMapper mapper)
        {
            _dictionaryService = dictionaryService;
            _mapper = mapper;
        }

        [HttpGet("lemma/{word}")]
        public async Task<ActionResult<List<LemmaDto>>> LookupLemma([Required] string word)
        {
            var result = await _dictionaryService.LookupLemma(word.ToLowerInvariant());
            if (result == null)
            {
                return NotFound();
            }

            var dto = _mapper.Map<List<LemmaResult>, List<LemmaDto>>(result.Results);
            return Ok(dto);
        }

        [HttpGet("entry/{word}")]
        public async Task<ActionResult<List<WordEntryDto>>> LookupEntry([Required] string word)
        {
            var result = await _dictionaryService.LookupEntry(word.ToLowerInvariant());
            if (result == null)
            {
                return NotFound();
            }

            var dto = _mapper.Map<List<EntryResult>, List<WordEntryDto>>(result.Results);
            return Ok(dto);
        }
    }
}