import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
	state: {
		searchTerm: null,
		searchOptions: { dictionary: true, wikipedia: true },
		dictionaryEntry: null,
		wikipediaArticle: null
	},
	mutations: {
		setSearchOptions(state, options) {
			state.searchOptions = options;
		},
		setSearchTerm(state, term) {
			state.searchTerm = term;
		},
		setDictEntry(state, entry) {
			state.dictionaryEntry = entry;
		},
		setWikiArticle(state, article) {
			state.wikipediaArticle = article;
		}
	},
	actions: {},
	modules: {}
});
