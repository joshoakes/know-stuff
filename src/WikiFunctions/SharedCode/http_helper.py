import azure.functions as func

urlDenyList = ["Wikiquote-logo.svg",
               "Symbol_book_class2.svg",
               "Wiktionary-logo-en-v2.svg",
               "Wikiversity-logo-en.svg",
               "Wikinews-logo.svg",
               "Wikivoyage-Logo-v3-icon.svg"]


def find_parameter_value(req: func.HttpRequest, param: str) -> str:
    value = req.params.get(param)

    if not value:
        try:
            req_body = req.get_json()
        except ValueError:
            pass
        else:
            value = req_body.get(param)

    return value


def bad_request(message: str) -> func.HttpResponse:
    return func.HttpResponse(message, status_code=400)


def not_found(message: str) -> func.HttpResponse:
    return func.HttpResponse(body=message, status_code=404)


def conflict(options) -> func.HttpResponse:
    return func.HttpResponse(body=options, status_code=409)
