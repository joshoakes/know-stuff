import logging
import azure.functions as func
import wikipedia


def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')

    query = req.params.get('query')
    if not query:
        try:
            req_body = req.get_json()
        except ValueError:
            pass
        else:
            query = req_body.get('query')

    if query:        
        try:
            summary = wikipedia.summary(query)
            return func.HttpResponse(summary)
        except (wikipedia.exceptions.DisambiguationError, wikipedia.exceptions.PageError):
            return func.HttpResponse(body="Dinner is ready and I'm still coding", status_code=404)
    else:
        return func.HttpResponse(
             "This HTTP triggered function executed successfully. Pass a name in the query string or in the request body for a personalized response.",
             status_code=400
        )
