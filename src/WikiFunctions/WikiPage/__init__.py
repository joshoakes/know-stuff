import logging
import wikipedia
import json
import azure.functions as func
from ..SharedCode import http_helper as helper


def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')

    title = helper.find_parameter_value(req, 'title')
    if not title:
        return helper.bad_request("Pass a title in the query string or in the request body for a personalized response.")

    try:
        page = wikipedia.page(title=title)
    except wikipedia.exceptions.PageError:
        return helper.not_found(f"Page not found: {title}")
    except wikipedia.exceptions.DisambiguationError as ex:
        dis_error = {
            "title": ex.title,
            "options": ex.options
        }
        return helper.conflict(json.dumps(dis_error))

    clean_ends = page.summary.strip()
    lines = clean_ends.splitlines()

    article_images = []
    try:
        article_images = filter(filterImageUrls, page.images)
    except KeyError:
        logging.error(f"Image KeyError {title} at {page.url}")
    else:
        logging.error(f"Could not get images for {title} at {page.url}")

    result = {
        "title": page.title,
        "url": page.url,
        "images": list(article_images),
        "summary": lines,
        "links": page.links
    }

    return func.HttpResponse(json.dumps(result))


def filterImageUrls(url):
    if url.startswith("https://upload.wikimedia.org/wikipedia/commons"):
        if any(imgName in url for imgName in helper.urlDenyList):
            return False
        else:
            return True

    return False
